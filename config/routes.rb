Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get '/', to: 'instruction#index'
  get '/test', to: 'instruction#index'
  get '/example', to: 'example#index'
  get '/hidden', to: 'professor#index'
  post '/', to: 'instruction#index'
  resources :users


end