class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :surname
      t.string :email
      t.string :phone
      t.text :work
      t.text :address
      t.string :relationship
      t.text :location
      t.text :photo

      t.timestamps
    end
  end
end
